package net

import (
	"fmt"
	"io"
	"net/http"
	"os"

	okio "gitlab.com/omnifi/automaatio/io"
)

// Download begins downloading a resource and returns a reader for the
// download.
func Download(url string) (io.ReadCloser, error) {
	// Get the data
	response, err := http.Get(url)

	if err != nil {
		return nil, err
	}

	if response.StatusCode == http.StatusOK {
		return response.Body, nil
	} else {
		return nil, &NetError{fmt.Sprintf("cannot download from '%s' – %v: %s", url, response.StatusCode, response.Status)}
	}

	return nil, nil
}

// DownloadAsFile will download a url to a local file. It's efficient because it will
// write as it downloads and not load the whole file into memory.
//
// Returns the file path if successful.
func DownloadAsFile(filepath string, url string) (string, error) {

	var out *os.File
	var err error

	body, err := Download(url)

	if err != nil {
		return "", err
	}

	defer body.Close()

	if len(filepath) == 0 {
		out, err = okio.GetTempFile("deus-")
	} else {
		// Create the file
		out, err = os.Create(filepath)
	}

	if err != nil {
		return "", err
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, body)
	return out.Name(), err
}

// DownloadAsString downloads the contents of the URL as a string.
func DownloadAsString(url string) (string, error) {

	body, err := Download(url)

	if err != nil {
		return "", err
	}

	defer body.Close()

	bodyBytes, err := io.ReadAll(body)
	if err != nil {
		return "", err
	}

	bodyString := string(bodyBytes)

	return bodyString, err
}
