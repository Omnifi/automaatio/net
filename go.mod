module gitlab.com/omnifi/automaatio/net

go 1.21.1

require (
	gitlab.com/omnifi/automaatio/io v0.0.0
)

replace gitlab.com/omnifi/automaatio/io => gitlab.com/omnifi/automaatio/io.git v0.0.0
