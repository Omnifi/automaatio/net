package net

import (
	"fmt"
)

// NetError is used to signal a failed network operation.
type NetError struct {
	// Error message.
	message string
}

// Returns the error message for network operations.
func (ioe *NetError) Error() string {
	return fmt.Sprintf("net error: %v", ioe.message)
}
